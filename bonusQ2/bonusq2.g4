grammar bonusq2;

@members
{
    void print(String line)
    {
        System.out.print(line);
    }

    void indent()
    {
        for (int i = 0; i < level * space; i++)
        {
            System.out.print(" ");
        }
    }

    void println(String line)
    {
        System.out.println(line);
    }

    int level = 0;
    int space = 2;
}

r : program {System.out.println(level);};

ident : Letter {print( " " + $Letter.text + " ");};

integer : Digit {print($Digit.text);} 
    (Digit {print($Digit.text);} )*;

selector : ('.' {print(".");} ident | 
    '[' {print("[");} 
    expression 
    ']' {print("]");} )*;

factor : 
    ident selector | integer | 
    '(' {print("(");} expression ')' {print(")");}  |
     'not' {print("not");} factor;

term : 
    factor 
    (('*' {print("*");} | 'div' {print("div");}  | 'mod' {print("mod");}  | 'and' {print("and");} ) factor)*;

simpleExpression : 
    ('+' {print("+");} | '-' {print("-");} )? 
    term 
    (('+' {print("+");}  | '-' {print("-");}  | 'or' {print("or");} ) term)*;

SymbolExpression : ('=' | '<>' | '<' | '<=' | '>' | '>=');

expression : simpleExpression ( SymbolExpression {print($SymbolExpression.text);} simpleExpression)*;

assignment : {indent();} ident selector ':=' {print(":=");} expression;

actualParameters : '(' {print("(");}
    (expression 
    (',' {print(",");} expression)*)? 
    ')' {print(")");};

procedureCall : {indent();} ident selector (actualParameters)?;

compoundStatement : 
    'begin' {indent(); println("begin"); level += 1;} 
    statement (';' {println(";");} statement)*  {println("");}
    'end' {level -= 1; indent(); print("end");};

ifStatement : 'if' {indent(); print("if"); level += 1;} 
    expression 
    'then' {println("then");}
    statement 
    {level -= 1;}
    ('else' {indent(); println("else"); level += 1;} statement {level -= 1;})?;

whileStatement : 'while' {indent(); print("while");} expression 'do' {println("do"); level += 1;} statement {level -=1 ;};

statement : (assignment | procedureCall   | compoundStatement  | ifStatement | whileStatement)?  ;

identList : ident (',' {print(",");} ident)* ;

arrayType : 'array' '[' {print("arrray [");} 
    expression 
    '..'  {print("..");}
    expression 
    ']' 'of' {print("] of ");} 
    type;

fieldList : (identList ':' {print(":");} type )?;

recordType : 'record' {print("record");}
     fieldList (';' {print("; ");} fieldList)* 
     'end' {print("end");};

type : ident | arrayType | recordType;

fPSection : 
    ('var' {print("var");})? identList ':' {print(":");} type;

formalParameters : 
    '(' {print("(");} (fPSection (';' {print(";");} fPSection )*)? ')' {print(")");};

procedureDelcaration : 
    'procedure' {print("procedure");} ident 
     (formalParameters)? ';' {println(";");} 
     declarations compoundStatement;

declarations : 
    (
        {indent();}
        'const' {print("const");}
            (ident '=' {print("=");} expression ';' {println(";");} )*
    )? 
    (
        {indent();}
        'type' {print("type");}
        (ident '=' {print("=");} type ';' {println(";");} )*
    )?
    (
        {indent(); level += 1;}
        'var' {println("var");}
        ({indent();} identList ':' {print(":");} type ';' {println(";");})*
        {level -= 1;}
    )? 
    (
        {indent();}
        procedureDelcaration ';' {println(";");}
    )*;

program : 
    'program' {print("program");} 
    ident 
    (
        '(' {print("(");}
        ident 
        (
            ',' {print(",");}
            ident
        )* 
        ')' {print(")");}
    )?  
    ';' {println(";"); level += 1;}
    declarations 
    compoundStatement {println("");};

Letter :
    [A-Za-z][a-z0-9]*;             

Digit:
    [0-9];

WS :
    [ \t\r\n]+ -> skip ; 

