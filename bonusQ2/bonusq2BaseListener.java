// Generated from bonusq2.g4 by ANTLR 4.0

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.antlr.v4.runtime.tree.ErrorNode;

public class bonusq2BaseListener implements bonusq2Listener {
	@Override public void enterExpression(bonusq2Parser.ExpressionContext ctx) { }
	@Override public void exitExpression(bonusq2Parser.ExpressionContext ctx) { }

	@Override public void enterSelector(bonusq2Parser.SelectorContext ctx) { }
	@Override public void exitSelector(bonusq2Parser.SelectorContext ctx) { }

	@Override public void enterProcedureDelcaration(bonusq2Parser.ProcedureDelcarationContext ctx) { }
	@Override public void exitProcedureDelcaration(bonusq2Parser.ProcedureDelcarationContext ctx) { }

	@Override public void enterArrayType(bonusq2Parser.ArrayTypeContext ctx) { }
	@Override public void exitArrayType(bonusq2Parser.ArrayTypeContext ctx) { }

	@Override public void enterType(bonusq2Parser.TypeContext ctx) { }
	@Override public void exitType(bonusq2Parser.TypeContext ctx) { }

	@Override public void enterIdent(bonusq2Parser.IdentContext ctx) { }
	@Override public void exitIdent(bonusq2Parser.IdentContext ctx) { }

	@Override public void enterRecordType(bonusq2Parser.RecordTypeContext ctx) { }
	@Override public void exitRecordType(bonusq2Parser.RecordTypeContext ctx) { }

	@Override public void enterDeclarations(bonusq2Parser.DeclarationsContext ctx) { }
	@Override public void exitDeclarations(bonusq2Parser.DeclarationsContext ctx) { }

	@Override public void enterActualParameters(bonusq2Parser.ActualParametersContext ctx) { }
	@Override public void exitActualParameters(bonusq2Parser.ActualParametersContext ctx) { }

	@Override public void enterCompoundStatement(bonusq2Parser.CompoundStatementContext ctx) { }
	@Override public void exitCompoundStatement(bonusq2Parser.CompoundStatementContext ctx) { }

	@Override public void enterFieldList(bonusq2Parser.FieldListContext ctx) { }
	@Override public void exitFieldList(bonusq2Parser.FieldListContext ctx) { }

	@Override public void enterInteger(bonusq2Parser.IntegerContext ctx) { }
	@Override public void exitInteger(bonusq2Parser.IntegerContext ctx) { }

	@Override public void enterFactor(bonusq2Parser.FactorContext ctx) { }
	@Override public void exitFactor(bonusq2Parser.FactorContext ctx) { }

	@Override public void enterIfStatement(bonusq2Parser.IfStatementContext ctx) { }
	@Override public void exitIfStatement(bonusq2Parser.IfStatementContext ctx) { }

	@Override public void enterStatement(bonusq2Parser.StatementContext ctx) { }
	@Override public void exitStatement(bonusq2Parser.StatementContext ctx) { }

	@Override public void enterAssignment(bonusq2Parser.AssignmentContext ctx) { }
	@Override public void exitAssignment(bonusq2Parser.AssignmentContext ctx) { }

	@Override public void enterIdentList(bonusq2Parser.IdentListContext ctx) { }
	@Override public void exitIdentList(bonusq2Parser.IdentListContext ctx) { }

	@Override public void enterTerm(bonusq2Parser.TermContext ctx) { }
	@Override public void exitTerm(bonusq2Parser.TermContext ctx) { }

	@Override public void enterR(bonusq2Parser.RContext ctx) { }
	@Override public void exitR(bonusq2Parser.RContext ctx) { }

	@Override public void enterWhileStatement(bonusq2Parser.WhileStatementContext ctx) { }
	@Override public void exitWhileStatement(bonusq2Parser.WhileStatementContext ctx) { }

	@Override public void enterFormalParameters(bonusq2Parser.FormalParametersContext ctx) { }
	@Override public void exitFormalParameters(bonusq2Parser.FormalParametersContext ctx) { }

	@Override public void enterProgram(bonusq2Parser.ProgramContext ctx) { }
	@Override public void exitProgram(bonusq2Parser.ProgramContext ctx) { }

	@Override public void enterFPSection(bonusq2Parser.FPSectionContext ctx) { }
	@Override public void exitFPSection(bonusq2Parser.FPSectionContext ctx) { }

	@Override public void enterSimpleExpression(bonusq2Parser.SimpleExpressionContext ctx) { }
	@Override public void exitSimpleExpression(bonusq2Parser.SimpleExpressionContext ctx) { }

	@Override public void enterProcedureCall(bonusq2Parser.ProcedureCallContext ctx) { }
	@Override public void exitProcedureCall(bonusq2Parser.ProcedureCallContext ctx) { }

	@Override public void enterEveryRule(ParserRuleContext ctx) { }
	@Override public void exitEveryRule(ParserRuleContext ctx) { }
	@Override public void visitTerminal(TerminalNode node) { }
	@Override public void visitErrorNode(ErrorNode node) { }
}