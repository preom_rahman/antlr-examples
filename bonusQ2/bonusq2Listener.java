// Generated from bonusq2.g4 by ANTLR 4.0
import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.Token;

public interface bonusq2Listener extends ParseTreeListener {
	void enterExpression(bonusq2Parser.ExpressionContext ctx);
	void exitExpression(bonusq2Parser.ExpressionContext ctx);

	void enterSelector(bonusq2Parser.SelectorContext ctx);
	void exitSelector(bonusq2Parser.SelectorContext ctx);

	void enterProcedureDelcaration(bonusq2Parser.ProcedureDelcarationContext ctx);
	void exitProcedureDelcaration(bonusq2Parser.ProcedureDelcarationContext ctx);

	void enterArrayType(bonusq2Parser.ArrayTypeContext ctx);
	void exitArrayType(bonusq2Parser.ArrayTypeContext ctx);

	void enterType(bonusq2Parser.TypeContext ctx);
	void exitType(bonusq2Parser.TypeContext ctx);

	void enterIdent(bonusq2Parser.IdentContext ctx);
	void exitIdent(bonusq2Parser.IdentContext ctx);

	void enterRecordType(bonusq2Parser.RecordTypeContext ctx);
	void exitRecordType(bonusq2Parser.RecordTypeContext ctx);

	void enterDeclarations(bonusq2Parser.DeclarationsContext ctx);
	void exitDeclarations(bonusq2Parser.DeclarationsContext ctx);

	void enterActualParameters(bonusq2Parser.ActualParametersContext ctx);
	void exitActualParameters(bonusq2Parser.ActualParametersContext ctx);

	void enterCompoundStatement(bonusq2Parser.CompoundStatementContext ctx);
	void exitCompoundStatement(bonusq2Parser.CompoundStatementContext ctx);

	void enterFieldList(bonusq2Parser.FieldListContext ctx);
	void exitFieldList(bonusq2Parser.FieldListContext ctx);

	void enterInteger(bonusq2Parser.IntegerContext ctx);
	void exitInteger(bonusq2Parser.IntegerContext ctx);

	void enterFactor(bonusq2Parser.FactorContext ctx);
	void exitFactor(bonusq2Parser.FactorContext ctx);

	void enterIfStatement(bonusq2Parser.IfStatementContext ctx);
	void exitIfStatement(bonusq2Parser.IfStatementContext ctx);

	void enterStatement(bonusq2Parser.StatementContext ctx);
	void exitStatement(bonusq2Parser.StatementContext ctx);

	void enterAssignment(bonusq2Parser.AssignmentContext ctx);
	void exitAssignment(bonusq2Parser.AssignmentContext ctx);

	void enterIdentList(bonusq2Parser.IdentListContext ctx);
	void exitIdentList(bonusq2Parser.IdentListContext ctx);

	void enterTerm(bonusq2Parser.TermContext ctx);
	void exitTerm(bonusq2Parser.TermContext ctx);

	void enterR(bonusq2Parser.RContext ctx);
	void exitR(bonusq2Parser.RContext ctx);

	void enterWhileStatement(bonusq2Parser.WhileStatementContext ctx);
	void exitWhileStatement(bonusq2Parser.WhileStatementContext ctx);

	void enterFormalParameters(bonusq2Parser.FormalParametersContext ctx);
	void exitFormalParameters(bonusq2Parser.FormalParametersContext ctx);

	void enterProgram(bonusq2Parser.ProgramContext ctx);
	void exitProgram(bonusq2Parser.ProgramContext ctx);

	void enterFPSection(bonusq2Parser.FPSectionContext ctx);
	void exitFPSection(bonusq2Parser.FPSectionContext ctx);

	void enterSimpleExpression(bonusq2Parser.SimpleExpressionContext ctx);
	void exitSimpleExpression(bonusq2Parser.SimpleExpressionContext ctx);

	void enterProcedureCall(bonusq2Parser.ProcedureCallContext ctx);
	void exitProcedureCall(bonusq2Parser.ProcedureCallContext ctx);
}