grammar bonusq3;
@init
{
}
@members
{
    int count = 0;
    String data = "";
}

r : table {
        for(int i=0;i<$table.values.size();i++){
                if ($table.isCellData.get(i) == "True")
                {
                    String cellValue = $table.values.get(i);
                    int diff = $table.size - cellValue.length();
                    for (int j=0; j < diff; j++)
                    {
                        cellValue = cellValue + "_";
                    }

                    System.out.print(cellValue);
                }
                else
                {
                    System.out.print($table.values.get(i));
                }
        } 
    };

table returns [int size, List<String> values, List<String> isCellData]
    locals [int i = 0]

    @init
    {
        $values = new ArrayList<String>();
        $isCellData = new ArrayList<String>(); 
    }

    : '<table>' (row
    {
        $values.addAll($row.values);
        $isCellData.addAll($row.isCellData);

        if ($row.size > $i)
        {
            $i = $row.size;
        }
        $size = $i;

        $values.add("\n");    
        $isCellData.add("False");    

    })* '</table>';


row returns [int size, List<String> values, List<String> isCellData]
    locals [int i = 0]

    @init
    {
        $values = new ArrayList<String>();
        $isCellData = new ArrayList<String>(); 
    }

    : '<tr>' 
    {
        $values.add(" | ");    
        $isCellData.add("False");    
    }

    (cell
    {
        if ($cell.size > $i)
        {
            $i = $cell.size;
        }
        $size = $i;

        $values.add($cell.value);    
        $isCellData.add("True");    

        $values.add(" | ");    
        $isCellData.add("False");    

    } )* '</tr>' ;

cell returns [int size, String value]

    : '<td>' ID '</td>'

    {
        $size = $ID.text.length();
        count += 1;
        $value = $ID.text;
    };

ID :
    [a-z]+ ;             

WS :
    [ \t\r\n]+ -> skip ; 
