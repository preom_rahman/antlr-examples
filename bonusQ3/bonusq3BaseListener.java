// Generated from bonusq3.g4 by ANTLR 4.0

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.antlr.v4.runtime.tree.ErrorNode;

public class bonusq3BaseListener implements bonusq3Listener {
	@Override public void enterR(bonusq3Parser.RContext ctx) { }
	@Override public void exitR(bonusq3Parser.RContext ctx) { }

	@Override public void enterCell(bonusq3Parser.CellContext ctx) { }
	@Override public void exitCell(bonusq3Parser.CellContext ctx) { }

	@Override public void enterTable(bonusq3Parser.TableContext ctx) { }
	@Override public void exitTable(bonusq3Parser.TableContext ctx) { }

	@Override public void enterRow(bonusq3Parser.RowContext ctx) { }
	@Override public void exitRow(bonusq3Parser.RowContext ctx) { }

	@Override public void enterEveryRule(ParserRuleContext ctx) { }
	@Override public void exitEveryRule(ParserRuleContext ctx) { }
	@Override public void visitTerminal(TerminalNode node) { }
	@Override public void visitErrorNode(ErrorNode node) { }
}