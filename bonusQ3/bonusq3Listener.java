// Generated from bonusq3.g4 by ANTLR 4.0
import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.Token;

public interface bonusq3Listener extends ParseTreeListener {
	void enterR(bonusq3Parser.RContext ctx);
	void exitR(bonusq3Parser.RContext ctx);

	void enterCell(bonusq3Parser.CellContext ctx);
	void exitCell(bonusq3Parser.CellContext ctx);

	void enterTable(bonusq3Parser.TableContext ctx);
	void exitTable(bonusq3Parser.TableContext ctx);

	void enterRow(bonusq3Parser.RowContext ctx);
	void exitRow(bonusq3Parser.RowContext ctx);
}