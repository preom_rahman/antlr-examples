// Generated from bonusq3.g4 by ANTLR 4.0
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class bonusq3Parser extends Parser {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__5=1, T__4=2, T__3=3, T__2=4, T__1=5, T__0=6, ID=7, WS=8;
	public static final String[] tokenNames = {
		"<INVALID>", "'</tr>'", "'<tr>'", "'</td>'", "'<table>'", "'<td>'", "'</table>'", 
		"ID", "WS"
	};
	public static final int
		RULE_r = 0, RULE_table = 1, RULE_row = 2, RULE_cell = 3;
	public static final String[] ruleNames = {
		"r", "table", "row", "cell"
	};

	@Override
	public String getGrammarFileName() { return "bonusq3.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public ATN getATN() { return _ATN; }


	    int count = 0;
	    String data = "";

	public bonusq3Parser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class RContext extends ParserRuleContext {
		public TableContext table;
		public TableContext table() {
			return getRuleContext(TableContext.class,0);
		}
		public RContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_r; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof bonusq3Listener ) ((bonusq3Listener)listener).enterR(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof bonusq3Listener ) ((bonusq3Listener)listener).exitR(this);
		}
	}

	public final RContext r() throws RecognitionException {
		RContext _localctx = new RContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_r);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(8); ((RContext)_localctx).table = table();

			        for(int i=0;i<((RContext)_localctx).table.values.size();i++){
			                if (((RContext)_localctx).table.isCellData.get(i) == "True")
			                {
			                    String cellValue = ((RContext)_localctx).table.values.get(i);
			                    int diff = ((RContext)_localctx).table.size - cellValue.length();
			                    for (int j=0; j < diff; j++)
			                    {
			                        cellValue = cellValue + "_";
			                    }

			                    System.out.print(cellValue);
			                }
			                else
			                {
			                    System.out.print(((RContext)_localctx).table.values.get(i));
			                }
			        } 
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TableContext extends ParserRuleContext {
		public int size;
		public List<String> values;
		public List<String> isCellData;
		public int i = 0;
		public RowContext row;
		public RowContext row(int i) {
			return getRuleContext(RowContext.class,i);
		}
		public List<RowContext> row() {
			return getRuleContexts(RowContext.class);
		}
		public TableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof bonusq3Listener ) ((bonusq3Listener)listener).enterTable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof bonusq3Listener ) ((bonusq3Listener)listener).exitTable(this);
		}
	}

	public final TableContext table() throws RecognitionException {
		TableContext _localctx = new TableContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_table);

		        ((TableContext)_localctx).values =  new ArrayList<String>();
		        ((TableContext)_localctx).isCellData =  new ArrayList<String>(); 
		    
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(11); match(4);
			setState(17);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==2) {
				{
				{
				setState(12); ((TableContext)_localctx).row = row();

				        _localctx.values.addAll(((TableContext)_localctx).row.values);
				        _localctx.isCellData.addAll(((TableContext)_localctx).row.isCellData);

				        if (((TableContext)_localctx).row.size > _localctx.i)
				        {
				            ((TableContext)_localctx).i =  ((TableContext)_localctx).row.size;
				        }
				        ((TableContext)_localctx).size =  _localctx.i;

				        _localctx.values.add("\n");    
				        _localctx.isCellData.add("False");    

				    
				}
				}
				setState(19);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(20); match(6);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RowContext extends ParserRuleContext {
		public int size;
		public List<String> values;
		public List<String> isCellData;
		public int i = 0;
		public CellContext cell;
		public CellContext cell(int i) {
			return getRuleContext(CellContext.class,i);
		}
		public List<CellContext> cell() {
			return getRuleContexts(CellContext.class);
		}
		public RowContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_row; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof bonusq3Listener ) ((bonusq3Listener)listener).enterRow(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof bonusq3Listener ) ((bonusq3Listener)listener).exitRow(this);
		}
	}

	public final RowContext row() throws RecognitionException {
		RowContext _localctx = new RowContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_row);

		        ((RowContext)_localctx).values =  new ArrayList<String>();
		        ((RowContext)_localctx).isCellData =  new ArrayList<String>(); 
		    
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(22); match(2);

			        _localctx.values.add(" | ");    
			        _localctx.isCellData.add("False");    
			    
			setState(29);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==5) {
				{
				{
				setState(24); ((RowContext)_localctx).cell = cell();

				        if (((RowContext)_localctx).cell.size > _localctx.i)
				        {
				            ((RowContext)_localctx).i =  ((RowContext)_localctx).cell.size;
				        }
				        ((RowContext)_localctx).size =  _localctx.i;

				        _localctx.values.add(((RowContext)_localctx).cell.value);    
				        _localctx.isCellData.add("True");    

				        _localctx.values.add(" | ");    
				        _localctx.isCellData.add("False");    

				    
				}
				}
				setState(31);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(32); match(1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CellContext extends ParserRuleContext {
		public int size;
		public String value;
		public Token ID;
		public TerminalNode ID() { return getToken(bonusq3Parser.ID, 0); }
		public CellContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cell; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof bonusq3Listener ) ((bonusq3Listener)listener).enterCell(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof bonusq3Listener ) ((bonusq3Listener)listener).exitCell(this);
		}
	}

	public final CellContext cell() throws RecognitionException {
		CellContext _localctx = new CellContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_cell);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(34); match(5);
			setState(35); ((CellContext)_localctx).ID = match(ID);
			setState(36); match(3);

			        ((CellContext)_localctx).size =  (((CellContext)_localctx).ID!=null?((CellContext)_localctx).ID.getText():null).length();
			        count += 1;
			        ((CellContext)_localctx).value =  (((CellContext)_localctx).ID!=null?((CellContext)_localctx).ID.getText():null);
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\2\3\n*\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\3\2\3\2\3\2\3\3\3\3\3\3\3\3\7"+
		"\3\22\n\3\f\3\16\3\25\13\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\7\4\36\n\4\f\4"+
		"\16\4!\13\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\2\6\2\4\6\b\2\2\'\2\n\3\2"+
		"\2\2\4\r\3\2\2\2\6\30\3\2\2\2\b$\3\2\2\2\n\13\5\4\3\2\13\f\b\2\1\2\f\3"+
		"\3\2\2\2\r\23\7\6\2\2\16\17\5\6\4\2\17\20\b\3\1\2\20\22\3\2\2\2\21\16"+
		"\3\2\2\2\22\25\3\2\2\2\23\21\3\2\2\2\23\24\3\2\2\2\24\26\3\2\2\2\25\23"+
		"\3\2\2\2\26\27\7\b\2\2\27\5\3\2\2\2\30\31\7\4\2\2\31\37\b\4\1\2\32\33"+
		"\5\b\5\2\33\34\b\4\1\2\34\36\3\2\2\2\35\32\3\2\2\2\36!\3\2\2\2\37\35\3"+
		"\2\2\2\37 \3\2\2\2 \"\3\2\2\2!\37\3\2\2\2\"#\7\3\2\2#\7\3\2\2\2$%\7\7"+
		"\2\2%&\7\t\2\2&\'\7\5\2\2\'(\b\5\1\2(\t\3\2\2\2\4\23\37";
	public static final ATN _ATN =
		ATNSimulator.deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
	}
}